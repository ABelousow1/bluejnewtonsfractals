import java.lang.Math;
/**
 * Exemplare dieser Klasse veranlassen SE1Turtles dazu,
 * Schneeflocken-Spuren auf einer Zeichenfläche zu hinterlassen.
 *
 * @author Till Aust, Axel Schmolitzky
 * @version 2005.11.26
 */
class Dompteur
{
    // Hier ist einstellbar, wie lange der Schneesturm dauert.
    private static final int STURM_DAUER = 300;

    /**
     * startet fuer ein paar Sekunden ein kleines Schneegestoeber mit 30 Schneeflocken
     */
    public Dompteur()
    {
        leiseRieseltDerSchnee();
    }
    public void leiseRieseltDerSchnee()
    {
        // Ein paar "zufaellig" verteilte Schneeflocken (manuell platziert)
        NewtonFractal nf = new NewtonFractal(3);
        
        //Math.pow(i, 1)/6, Math.pow(i, 1)/6
        //double i = 5;
        for (double i = 0.0; i <= STURM_DAUER; i += 0.1) {
            nf.drawFractal(0.0, 0.333, 1.0 + i*i, i, 0.5*i/STURM_DAUER);
        }
    }

}