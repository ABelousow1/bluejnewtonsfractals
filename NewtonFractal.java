
/**
 * Write a description of class NewtonFractal here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NewtonFractal
{
    // instance variables - replace the example below with your own
    private ColorFinder _colorFinder;
    private Turtle paintTurtle;

    /**
     * Constructor for objects of class NewtonFractal
     */
    public NewtonFractal(int powerOfPolynom)
    {
        // initialise instance variables
        paintTurtle = new Turtle();
        paintTurtle.setzeGeschwindigkeit(10);
        paintTurtle.hinterlasseKeineSpur();
        _colorFinder = new ColorFinder(powerOfPolynom);
    }
    
    public NewtonFractal()
    {
        // initialise instance variables
        this(3);
    }
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public void drawFractal(double x, double y, double size, double angle)
    {
        drawFractal(x, y, size, angle, 0.005);
    }
    public void drawFractal(double x, double y, double size, double angle, double precision)
    {
        // put your code here
        //paintTurtle.loescheAlleSpuren();        
        for (double i = 0; i < 500; ++i)
        {
            paintTurtle.geheZu(0.0, i);
            paintTurtle.hinterlasseSpur();
            final double xim = ((i * 0.004 - 1.0)/size);
            for (double j = 0; j < 500; ++j)
            {
                final double yim = ((j * 0.004 - 1.0)/size);
                paintTurtle.setzeFarbe(_colorFinder.getColor(Math.cos(angle)*xim + 
                                                                Math.sin(angle)*yim + x, 
                                                               -Math.sin(angle)*xim + 
                                                                Math.cos(angle)*yim + y, precision));
                paintTurtle.geheZu(j, i);
            }
            paintTurtle.hinterlasseKeineSpur();
        }
    }
}
