import java.lang.Math;
/**
 * Class for representation of Newton fractal for z^3 - 1.
 * 
 * @author Arsenii Belousov 
 * @version 21.12.2022
 */
public class ColorFinder
{
    // instance variables - replace the example below with your own
    private final ComplexNumber roots[];
    private String colors[];
    private final int _power;
    
    ColorFinder(int power){
        _power = power;
        roots = new ComplexNumber[_power];
        for (int i = 0; i < _power; ++i)
        {
            roots[i] = new ComplexNumber(Math.cos((2*Math.PI * i) / _power), Math.sin((2*Math.PI * i)/ _power));
        }
        setColors(null);
    }
    
    public String findColor(ComplexNumber num, double eps){
        double mindist = eps;
        int mini = 0;
        for (int i = 0; i < _power; ++i)
        {
            double dist = num.sub(num, roots[i]).abs();
            
            if (dist < mindist)
            {
                mindist = dist;
                mini = i;
            }
        }
        return colors[mini];
    }
    /**
     * Constructor for objects of class ColorFinder
     */
    public ColorFinder()
    {
        this(3);
    }
    
    public ColorFinder(String[] colours)
    {
        this();
        colors = colours;
    }
    
    public void setColors(String[] colours)
    {
        colors = colours;
        if (colours == null)
        {
             colors = new String[]{"schwarz", "blau", "cyan", "dunkelgrau",
                                    "grau", "gruen", "hellgrau", "magenta", 
                                    "orange", "pink", "rot", "weiss", "gelb"};
        }
    }
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String getColor(double x, double y, double eps){
        //start approximation
        ComplexNumber z_1 = new ComplexNumber(x, y);
        ComplexNumber z_2 = z_1.sub(z_1, z_1.div(z_1.sub(z_1.pow(z_1, 3), new ComplexNumber(1)), z_1.mul(z_1.pow(z_1, 2), new ComplexNumber(3)))); 
        int N = 50;
        int i=0;
        while (z_1.sub(z_2, z_1).abs() > eps/5  && i++ < N)
        {
            z_1 = z_2;
            //z_1 = z_1 - (z_1^n - 1)/(n*z_1^(n-1));
            z_2 = z_1.sub(z_1, z_1.div(z_1.sub(z_1.pow(z_1, _power), new ComplexNumber(1)), z_1.mul(z_1.pow(z_1, _power - 1), new ComplexNumber(_power)))); 
        }
        return findColor(z_1, eps);
    }
}
