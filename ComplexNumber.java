import java.lang.Math;
/**
 * Implements the complex number with real and imaginary part.
 * 
 * @author Arsenii Belousov 
 * @version 21.12.2022
 */
public class ComplexNumber
{
    // real part
    private double _real;
    // imaginary part
    private double _imag;
    /**
     * Default constructor creates complex zero (0,0).
     */
    ComplexNumber()
    {
        _real = 0.0;
        _imag = 0.0;
    }

    /**
     * Constructor with one parametre creates the number z = (re, 0).
     * 
     * @param re the real part of created number, double  
     * 
     */
    public ComplexNumber(double re)
    {
        // initialise instance variables
        _real = re;
        _imag = 0.0;
    }
    
    /**
     * Constructor with two parametrs creates the number z = (re, im).
     * 
     * @param re the real part of created number, type double
     * @param im the imaginary part of created number, type double
     * 
     */
    public ComplexNumber(double re, double im)
    {
        this(re);
        _imag = im;
    }
    
    /**
     * Copy constructor.
     * 
     * @param num   ComplexNumber of whitch a copy should be created.
     * 
     */
    public ComplexNumber(ComplexNumber num)
    {
        this(num._real, num._imag);    
    }
    
    /**
     * Absolute value of complex number.
     * 
     * @return positive double |z|.
     */
    public double abs()
    {
        return Math.pow(_imag * _imag + _real * _real, 0.5);
    }
    
    /**
     * Angle in radians between x axis und complex number.
     * 
     * @return double arg(z) in range [-PI/2, 3PI/2).
     */
    public double angle()
    {
        if (_real >= 0)
            return Math.asin(_imag / this.abs());
        else
        {
            return Math.PI - Math.asin(_imag / this.abs());
        }
    }

    /**
     * Complex conjunction of current complex number.
     * 
     * @return complex number as conjunction of current.
     */
    public ComplexNumber conj()
    {
        return new ComplexNumber(_real, - _imag);
    }
    
     /**
     * real part of current complex number.
     * 
     * @return real part of complex number as double.
     */
    public double getReal()
    {
        return _real;
    }
    
     /**
     * imaginary part of current complex number.
     * 
     * @return real part of complex number as double.
     */
    public double getImag()
    {
        return _imag;
    }
    
    /**
     * Sets real part of current complex number.
     * 
     * @param real   a real part to set.
     */
    public void setReal(double real)
    {
        _real = real;
    }

     /**
     * Sets imaginary part of current complex number.
     * 
     * @param imag   an imaginary part to set.
     */
    public void setImag(double imag)
    {
        _imag = imag;
    }
    /**
     * Creates the new complex number as a product of two given complex numbers.
     * 
     * @param  x   first complex number
     * @param  y   second complex number
     * @return     the product of two given numbers.
     */
    public static ComplexNumber mul(ComplexNumber x, ComplexNumber y)
    {
        // put your code here
        return new ComplexNumber(x._real * y._real - x._imag * y._imag, 
                                 x._imag * y._real + y._imag * x._real);
    }
    
    /**
     * Creates the new complex number as a sum of two given complex numbers.
     * 
     * @param  x   first complex number
     * @param  y   second complex number
     * @return     the sum of two given numbers.
     */
    public static ComplexNumber sum(ComplexNumber x, ComplexNumber y)
    {
        return new ComplexNumber(x._real + y._real, x._imag + y._imag);
    }
    
    /**
     * Creates the new complex number as a subtraction of two given complex numbers.
     * 
     * @param  x   first complex number
     * @param  y   second complex number
     * @return     the difference between two given numbers z = (x - y).
     */
    public static ComplexNumber sub(ComplexNumber x, ComplexNumber y)
    {
        return new ComplexNumber(x._real - y._real, x._imag - y._imag);
    }
    
    /**
     * Creates the new complex number as a division of a given complex number and given real number.
     * 
     * @param  x   first ComplexNumber
     * @param  y   second real number as double
     * @return     the division between two given numbers z = (x/y).
     */
    public static ComplexNumber div_with_real(ComplexNumber x, double y)
    {
        return new ComplexNumber(x._real / y, x._imag / y);
    }
    
    /**
     * Creates the new complex number as a division of two given complex numbers.
     * 
     * @param  x   first complex number
     * @param  y   second complex number
     * @return     the division between two given numbers z = (x/y).
     */
    public static ComplexNumber div(ComplexNumber x, ComplexNumber y)
    {
        return div_with_real(mul(x, y.conj()), y.abs() * y.abs());
    }
    
    /**
     * Creates the new complex number as a power of the given one.
     * 
     * @param  x   complex number
     * @param  y   whole power (int)
     * @return     the power y of x, z = x^y.
     */
    public static ComplexNumber pow(ComplexNumber x, int y)
    {
        ComplexNumber cn = new ComplexNumber(1);
        for (int i = 0; i < y; ++i)
        {
            cn = mul(cn,x);
        }
        return cn;
    }
    
    /**
     * Checks if two complex numbers are equal.
     * 
     * @param  x   complex number
     * @param  y   complex number
     * @return     true if they have the same real and imaginary part.
     */
    public static boolean equals(ComplexNumber x, ComplexNumber y)
    {
        return x._real == y._real && x._imag == y._imag;
    }
    
    /**
     * Checks if the distance between two complex numbers are less than given eps.
     * 
     * @param  x   complex number
     * @param  y   complex number
     * @param eps  double epsilon to check the distance
     * @return     true if they have the same real and imaginary part.
     */
    public static boolean eqls(ComplexNumber x, ComplexNumber y, double eps)
    {
        return sub(x,y).abs() < eps;
    }
    
}
